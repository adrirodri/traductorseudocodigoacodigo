<html  lang="es">
<head>
  <meta charset="UTF-8">
  <title>Traductor a Perl</title>
</head>
<body>
<article style="padding-left:15%; padding-right:15%; text-align: justify;">
<?php
/*Traductor de pseudocódigo a código
Realizar un cliente y un servidor que permita la traducción de pseudocódigo a tres 
lenguajes de programación determinados. El lenguaje de pseudocódigo así como sus 
estructuras serán dadas por el profesor. Se valorará la solución aportada a nivel 
de servicio, a nivel de ESB así como la interfaz creada para el cliente.
*/

error_reporting(0);
$datos=json_decode(file_get_contents('php://input'),1);
//Se abrirá un archivo .txt que será el cual contenga el código para traducir


$fp = fopen($datos["fichero"], "r");
$separador = " "; //Separador de palabras (" ", ":")
$variables = array();

//var_dump($variables);

//Indicadores de qué parte del pseudocódigo se está procesando
$func = false;	
$par = false;
$var = false;
$ini = false;
$nivel=0;

echo '#!/usr/bin/perl';
echo "</br> </br> </br>";

while(!FEOF($fp))
{
$linea = fgets($fp);
$palabras = explode($separador,$linea);
$n = count($palabras);
tab($nivel);
$i=0;
	//for($i=0; $i<$n; $i++)
	while ($i<$n)
	{
		$palabras[$i] = trim($palabras[$i]);
	//echo $palabras[$i];
	
	////-----------------------------------SE PROCESA LA PALABRA------------------------------------------////////
	
		///------------------------------PARTE DE DEFINICIÓN DE LA FUNCIÓN-------------------------------///////
		if($palabras[$i]=="FUNCION")//Si se lee la palabra reservada FUNCION
		{
		$par = false;
		$var = false;
		$ini = false;
		$func =true;
		$separador = " ";
		
		echo "sub ";//Ya que en perl las funciones se definen así, la parte principal no va dentro de una función
		
		$n = count($palabras);
		$j=$i+1;
			while($j<$n)
			{
			$palabras[$j] = trim($palabras[$j]);
			
				if($j==$i+1)
				{
					
				$fun=$palabras[$j];
				
				$j=$j+1;
				$palabras[$j] = trim($palabras[$j]);
				
					if($palabras[$j]=="DEVUELVE")
					{
					$j=$j+1;
					$i=$n-1;
					}
					else//Algún tipo de error
					{
						
					}
				echo $fun;
				
				$j=$j+1;
				}
				else//Algún error
				{
					
				}
			}
		}
		
		if($palabras[$i]=="PARAMETROS")//Si estamos en la parte de los PARAMETROS
		{
		echo "</br>";
		echo "{";
		echo "</br>";
		$separador = ":";
		$func =false;
		$par = true;
		$var = false;
		$ini = false;
		
		$cuenta = 0;
		}
		else if ($par == true)
		{	
		error_reporting(0);
		
			if($palabras[$i]=="VARIABLES")
			{
			$par =false;
			}
			else
			{
				if($i%2==0)
				{
				$palabras[$i+1] = trim($palabras[$i+1]);
				 
				if($cuenta>0) echo "</br>";
				$cuenta = $cuenta +1;
				$variables[$palabras[$i]] = tipoPalabrasP($palabras[$i+1]);
				
				//echo $variables[$palabras[$i]];
				//echo $palabras[$i];*/
				}
				else
				{	
					if(compVariableTipo2($variables, $palabras[$i]) == false)
					{
					echo '$';
					echo $palabras[$i-1];
					}
					else
					{
					echo $palabras[$i-1];
					}
				echo ' = shift;';
				}
				
			}
		}
		
		
		///------------------------------PARTE DE DEFINICIÓN DE VARIABLES-------------------------------///////
		if($palabras[$i]=="VARIABLES")//Si estamos en la parte de las VARIABLES
		{
			$separador = ":";
			$func =false;
			$var = true;
			$par = false;
			$ini = false;
		}
		else if ($var == true)
		{	
		error_reporting(0);
		
			if($palabras[$i]=="INICIO")
			{
				$var =false;
				
			}
			else
			{
				$palabras[$i+1] = trim($palabras[$i+1]);
				if($i%2==0)
				{	
					if($palabras[$i]!="")	echo "$";
					$variables[$palabras[$i]] = tipoPalabrasP($palabras[$i+1]);
					echo $palabras[$i];
				}
				else
				{
				
				tipoPalabras($palabras[$i]);
				
				
				}
			}
		}	
		
		
		///------------------------------PARTE DE EJECUCIÓN DEL PROGRAMA-------------------------------///////
		if($palabras[$i]=="INICIO")//Si estamos en la parte de INICIO, es decir, la parte de ejecución del programa
		{
			$separador = " ";
			$func =false;
			$par = false;
			$var = false;
			$ini = true;
		}
		
		else if($ini == true)//Si se está procesando la parte entre INICIO y FIN
		{
			//PARTE REFERENTE AL ESCRIBIR-///
			if($palabras[$i]=="ESCRIBIR")
			{
			echo 'print "';
			$text = false;
			$n = count($palabras);
				for($j=($i+1); $j<$n; $j++)
				{
				$palabras[$j] = trim($palabras[$j]);
					
					if($palabras[$j]!='"')
					{
						if($text==true)
						{
						
						echo $palabras[$j];
						echo " ";
						}
						else
						{
							foreach($variables as $var=>$tipo)
							{
								if($palabras[$j] == $var)
								{
								tipos($tipo, $palabras[$j]);		
								}
								
								if(strpos($palabras[$j],$var)!==false)
								{
									if($tipo == "array")
									{
										if(strpos($palabras[$j],"[")!==false)
										{
										$arry = array();
										$arry=explode("[",$palabras[$j]);
										
										echo " $";
										echo ''.$arry[0].'[';
										
											if(compruebaVariable($variables, $arry[1])==true)
											{
											echo "$";
											}
								
										echo ''.$arry[1].'';
										echo " ";
										}
										else
										{
										echo " @";
										echo ''.$palabras[$j].' ';
										echo "";
										}
									}
								}

							}
						}
						
					}
					else
					{
						if($text == false)
						{
						$text = true;	
						}
						else
						{
						$text = false;	
						}
					}
				}
			
			//se ponen todos los nombres de las variables usadas en orden	
			echo ' \n";';
			$i=$n;
			}
			else if($palabras[$i]=="LEER") //Se supone que se tendrán que hacer formularios para leer
			{
			$palabras[$i+1] = trim($palabras[$i+1]);
			
				if(compruebaVariable($variables, $palabras[$i+1])==true)
				{
				echo '$'.$palabras[$i+1].'=&lt;STDIN&gt;;';
				}
				else
				{
				echo 'La palabra "'.$palabras[$i+1].'" no es una variable definida';
				}
						
			$i=$n;
			}
			
			//PARTE REFERENTE AL SI-///
			else if($palabras[$i]=="SI")
			{
			$nivel=$nivel+1;
			echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
			echo "if(";
				$n = count($palabras);
				for($j=($i+1); $j<$n; $j++)
				{
				$palabras[$j] = trim($palabras[$j]); 
				
					//se procesa cada palabra dentro del SI
					if($palabras[$j]=="=")
					{
					echo "==";
					}
					
					else if($palabras[$j]=="MOD")
					{
						echo "%";
					}
					else
					{
						if(compVariableTipo($variables, $palabras[$j]) == false)
						{
						echo $palabras[$j];
						}
			
					}
					if($j<($n-1)){echo " ";}
				}
			$i=$n;
			echo ")";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
			else if($palabras[$i]=="SINO")
			{
			echo "}";
			echo "</br>";
			tab($nivel);
			echo "else";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
			else if($palabras[$i]=="FIN-SI")
			{
			echo "}";
			$nivel=$nivel-1;
			}
			//PARTE REFERENTE AL PARA-///
			else if($palabras[$i]=="PARA")
			{
			$nivel=$nivel+1;
			echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
			echo "for(";
			
			//procesamos el PARA
			$n = count($palabras);
				for($j=($i+1); $j<$n; $j++)
				{
				$palabras[$j] = trim($palabras[$j]);	
				
					//se procesa cada palabra dentro del PARA
					if($palabras[$j]=="HASTA")
					{
						echo ";";
					}
					else if($palabras[$j]=="INCREMENTO")
					{
						echo "; $";
						echo $palabras[1];
						echo " = $";
						echo $palabras[1];
						echo " + ";
					}
					else if($palabras[$j]=="DECREMENTO")
					{
						echo "; $";
						echo $palabras[1];
						echo " = $";
						echo $palabras[1];
						echo " - ";
					}
					else if($palabras[$j]=="MOD")
					{
						echo "%";
					}
					else
					{
					
						if(compVariableTipo($variables, $palabras[$j]) == false)
						{
						echo $palabras[$j];
						}
						
					}
					if($j<($n-1)){echo " ";}
				}
			$i=$n;
			echo ")";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
			
			else if($palabras[$i]=="FIN-PARA")
			{
			echo "}";
			$nivel=$nivel-1;
			}
			
			
			//PARTE REFERENTE AL MIENTRAS///
			else if($palabras[$i]=="MIENTRAS")
			{
			$nivel=$nivel+1;
			echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
			echo "while(";
			$n = count($palabras);
			
				for($j=($i+1); $j<$n; $j++)
				{
				$palabras[$j] = trim($palabras[$j]); 
				
					//se procesa cada palabra dentro del MIENTRAS
					if($palabras[$j]=="=")
					{
					echo "==";
					}
					
					else if($palabras[$j]=="MOD")
					{
						echo "%";
					}
					else
					{
						if(compVariableTipo($variables, $palabras[$j]) == false)
						{
						echo $palabras[$j];
						}
					}
					if($j<($n-1)){echo " ";}
				}
			$i=$n;
			echo ")";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
				
			else if($palabras[$i]=="FIN-MIENTRAS")
			{
				echo "}";
				$nivel=$nivel-1;
			}
			//PARTE REFERENTE A LAS OPERACIONES
			
			else if($palabras[$i]=="<-")
			{
				echo " = ";
			}
			//si la operacion es MOD
			else if($palabras[$i]=="MOD")
			{
				echo " % ";
				
			}
			//Si leemos un SALIR
			else if($palabras[$i]=="SALIR")
			{
				echo "exit();";
			}
			else if($palabras[$i]=="DEVUELVE")
			{
				
			echo "return ";
			$i=$i+1;
			$palabras[$i] = trim($palabras[$i]);
			
				if(compVariableTipo($variables, $palabras[$i]) == false)
				{
				echo $palabras[$i];
				}
			echo ";";
				
			}
				
			///------------------------------SE LLEGA AL FINAL DE LA FUNCIÓN---------------------------------///////
			else if($palabras[$i]=="FIN")
			{
			//Para poner las llaves que queden por cerrar
			echo "}";
			}
			else 
			{
			//Hacer una comprobación de si es una variable
			$esArray = compVariableTipo($variables, $palabras[$i]);
				
			if($esArray == false)
			{
			echo $palabras[$i];
			echo " ";
			}
			
			$esArray= false; 
			
				if ($i==$n-1 && $i>1)
				{
					echo ";";
				}
				echo " ";
			}

		}	
	//echo "////";
	$i=$i+1;
	}


if($par!=true && $func!=true) echo "</br>";

//echo $linea;
}


//FUNCIÓN PARA TABULAR
function tab($nivel)
{
	for($z=1;$z<=$nivel;$z++)
	{
		//echo "<dd><dd>";
		//echo "a ";
		echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
		
		
	}
}


function compVariableTipo($variables, $palabra)
{
	foreach($variables as $var=>$tipo)
	{
		if($palabra == $var)
		{
			
			if($tipo == "int")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
			
			if($tipo == "char")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
			
			if($tipo == "float")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
			
			if($tipo == "string")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
		}
		
		if(strpos($palabra,$var)!==false)
		{
			if($tipo == "array")
			{
			
			$esArray= true; 
			
				if(strpos($palabra,"[")!==false)
				{
				$arry = array();
				$arry=explode("[",$palabra);
			
				echo " $";
				echo ''.$arry[0].'[';
				
					if(compruebaVariable($variables, $arry[1])==true)
					{
					echo "$";
					}
					
				echo ''.$arry[1].'';
				echo " ";
				}
				else
				{
				echo " @";
				echo $palabra;
				}
			}
		}
	}
return $esArray;
}



function compVariableTipo2($variables, $palabra)
{
	foreach($variables as $var=>$tipo)
	{
		if($palabra == $var)
		{
			
			if($tipo == "int")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
			
			if($tipo == "char")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
			
			if($tipo == "float")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
			
			if($tipo == "string")
			{
				if(strpos($palabra,"]")!==false)
				{
				}
				else
				{
				echo "$";
				}
			}
		}
		
		if(strpos($palabra,$var)!==false)
		{
			if($tipo == "array")
			{
			
			$esArray= true; 
			echo " @";
			}
		}
	}
return $esArray;
}

function compruebaVariable($variables, $pala)
{
$ret = 0;
$pala2=explode("]",$pala);

//echo $pala2[0];

	foreach($variables as $var=>$tipo)
	{

		if($pala2[0] == $var)
		{$ret = 1;}
	}
	
//echo $ret;

return $ret;
}





//FUNCIÓN PARA GUARDAR TODAS LAS VARIABLES CON SUS RESPECTIVOS TIPOS
function tipoPalabras($palabra)
{
	
	if($palabra=="ENTERO")
	{
		echo " = 0;";
		$tipo="int";
		
	}
	
	if($palabra=="CARACTER")
	{
		echo " = '';";
		$tipo="char";
		
	}
	
	if($palabra=="REAL")
	{
		echo " = 0;";
		$tipo="float";
		
	}
	
	if($palabra=="CADENA")
	{
		echo ' = "";';
		$tipo="string";
		
	}
	
	if(strpos($palabra,"ARRAY")!==false)
	{
		echo " = array();";
		$tipo="array";
	}
	
return $tipo;
}

function tipoPalabrasP($palabra)
{
	
	if($palabra=="ENTERO")
	{
		$tipo="int";
		
	}
	
	if($palabra=="CARACTER")
	{
		$tipo="char";
		
	}
	
	if($palabra=="REAL")
	{
		$tipo="float";
		
	}
	
	if($palabra=="CADENA")
	{
		$tipo="string";
		
	}
	
	if(strpos($palabra,"ARRAY")!==false)
	{
		$tipo="array";
	}
	
return $tipo;
}




function tipos($tipo, $palabra)
{
	if($tipo == "int")
	{
		if(strpos($palabra,"]")!==false)
		{
		}
		else
		{
		echo " $";
		echo ''.$palabra.' ';
		echo " ";
		
		}
	}

	if($tipo == "char")
	{
		if(strpos($palabra,"]")!==false)
		{
		}
		else
		{
		echo " $";
		echo ''.$palabra.' ';
		echo " ";
		
		}
	}

	if($tipo == "float")
	{
		if(strpos($palabra,"]")!==false)
		{
		}
		else
		{
		echo " $";
		echo ''.$palabra.' ';
		echo " ";
		
		}
	}
	
	if($tipo == "string")
	{
		if(strpos($palabra,"]")!==false)
		{
		}
		else
		{
		echo " $";
		echo ''.$palabra.' ';
		echo " ";
		
		}
	}
}
//var_dump($variables);
fclose($fp);
?>
</article>
</body>
</html>