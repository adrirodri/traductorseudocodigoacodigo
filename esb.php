<?php

//recibimos datos en formato json 
$datos=json_decode(file_get_contents('php://input'),1);

if($datos["fichero"] != NULL)
{
	if($datos["codigo"] =='C')
	{

		$data = array("fichero"=>$datos["fichero"]);
		 $handle = curl_init("http://localhost/PracticaSOA/traductorServC.php");
		 curl_setopt($handle, CURLOPT_POST, true);
		 curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
		 curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		 $response=curl_exec($handle);
		 curl_close($handle);
		 echo $response;

	}

	else if($datos["codigo"] =='PHP')
	{

		$data = array("fichero"=>$datos["fichero"]);
		 $handle = curl_init("http://localhost/PracticaSOA/traductorServPHP.php");
		 curl_setopt($handle, CURLOPT_POST, true);
		 curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
		 curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		  $response=curl_exec($handle);
		 curl_close($handle);
		 echo $response;
		 
	}

	else if($datos["codigo"] =='Perl')
	{

		$data = array("fichero"=>$datos["fichero"]);
		 $handle = curl_init("http://localhost/PracticaSOA/traductorServPerl.php");
		 curl_setopt($handle, CURLOPT_POST, true);
		 curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
		 curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		 $response=curl_exec($handle);
		 curl_close($handle);
		 echo $response;
		 
	}
}
else
{
 echo '<p style="text-align: center; color: red; font-size: 22pt;">Introduzca un fichero </p>';
}
?>