<html  lang="es">
<head>
  <meta charset="UTF-8">
  <title>Traductor a C</title>
</head>
<body>
<article style="padding-left:15%; padding-right:15%; text-align: justify;">
<?php
/*Traductor de pseudocódigo a código
Realizar un cliente y un servidor que permita la traducción de pseudocódigo a tres 
lenguajes de programación determinados. El lenguaje de pseudocódigo así como sus 
estructuras serán dadas por el profesor. Se valorará la solución aportada a nivel 
de servicio, a nivel de ESB así como la interfaz creada para el cliente.
*/

error_reporting(0);
$datos=json_decode(file_get_contents('php://input'),1);
//Se abrirá un archivo .txt que será el cual contenga el código para traducir


$fp = fopen($datos["fichero"], "r");
$separador = " "; //Separador de palabras (" ", ":")
$variables = array();

//var_dump($variables);

//Indicadores de qué parte del pseudocódigo se está procesando
$func = false;	
$par = false;
$var = false;
$ini = false;
$nivel=0;

echo '#include &lt;stdio.h&gt;';
echo "</br>";
echo '#include &lt;stdlib.h&gt;';
echo "</br> </br> </br>";

while(!FEOF($fp))
{
$linea = fgets($fp);
$palabras = explode($separador,$linea);
$n = count($palabras);
tab($nivel);
$i=0;
	//for($i=0; $i<$n; $i++)
	while ($i<$n)
	{
		$palabras[$i] = trim($palabras[$i]);
	//echo $palabras[$i];
	
	////-----------------------------------SE PROCESA LA PALABRA------------------------------------------////////
	
		///------------------------------PARTE DE DEFINICIÓN DE LA FUNCIÓN-------------------------------///////
		if($palabras[$i]=="FUNCION")//Si se lee la palabra reservada FUNCION
		{
		$par = false;
		$var = false;
		$ini = false;
		$func =true;
		$separador = " ";
		
		$n = count($palabras);
		$j=$i+1;
			while($j<$n)
			{
			$palabras[$j] = trim($palabras[$j]);
			
				if($j==$i+1)
				{
					if($palabras[$j]=="PRINCIPAL")//Si es la función pricipal
					{
					$j=$j+1;
					$palabras[$j] = trim($palabras[$j]);
					
						if($palabras[$j]=="DEVUELVE")
						{
						$j=$j+1;
						$palabras[$j] = trim($palabras[$j]);
						tipoPalabras($palabras[$j]);
						$i=$n-1;
						}
						else//Algún tipo de error
						{
							
						}
					echo "main(";
					
					}	
					else
					{
					$fun=$palabras[$j];
					
					$j=$j+1;
					$palabras[$j] = trim($palabras[$j]);
					
						if($palabras[$j]=="DEVUELVE")
						{
						$j=$j+1;
						$palabras[$j] = trim($palabras[$j]);
						tipoPalabras($palabras[$j]);
						$i=$n-1;
						}
						else//Algún tipo de error
						{
							
						}
					echo $fun;
					echo "(";
					}
				$j=$j+1;
				}
				else//Algún error
				{}
			}
		}
		
		if($palabras[$i]=="PARAMETROS")//Si estamos en la parte de los PARAMETROS
		{
		$separador = ":";
		$func =false;
		$par = true;
		$var = false;
		$ini = false;
		
		$cuenta = 0;
		}
		else if ($par == true)
		{	
		error_reporting(0);
		
			if($palabras[$i]=="VARIABLES")
			{
				cierreFun();
				$par =false;
			}
			else
			{
				if($i%2==0)
				{
				$palabras[$i+1] = trim($palabras[$i+1]);
				 
				if($cuenta>0) echo ", ";
				$cuenta = $cuenta +1;
				$variables[$palabras[$i]] = tipoPalabras($palabras[$i+1]);
				
				//echo $variables[$palabras[$i]];
				//echo $palabras[$i];
				}
				else
				{	
				echo $palabras[$i-1];

				}
				
			}
		}
		
		
		///------------------------------PARTE DE DEFINICIÓN DE VARIABLES-------------------------------///////
		if($palabras[$i]=="VARIABLES")//Si estamos en la parte de las VARIABLES
		{
			$separador = ":";
			$func =false;
			$var = true;
			$par = false;
			$ini = false;
		}
		else if ($var == true)
		{	
		error_reporting(0);
		
			if($palabras[$i]=="INICIO")
			{
				$var =false;
				
			}
			else
			{
				if($i%2==0)
				{
					$palabras[$i+1] = trim($palabras[$i+1]);
					
					$variables[$palabras[$i]] = tipoPalabras($palabras[$i+1]);
					//echo $variables[$palabras[$i]];
					//echo $palabras[$i];
				}
				else
				{
				echo $palabras[$i-1];
				echo ";";
				}
			}
		}
		
		
		
		///------------------------------PARTE DE EJECUCIÓN DEL PROGRAMA-------------------------------///////
		if($palabras[$i]=="INICIO")//Si estamos en la parte de INICIO, es decir, la parte de ejecución del programa
		{
			$separador = " ";
			$func =false;
			$par = false;
			$var = false;
			$ini = true;
		}
		
		else if($ini == true)//Si se está procesando la parte entre INICIO y FIN
		{
			//PARTE REFERENTE AL ESCRIBIR-///
			if($palabras[$i]=="ESCRIBIR")
			{
			echo 'printf("';
			$text = false;
			$vari = array();
			
			
				$n = count($palabras);
				for($j=($i+1); $j<$n; $j++)
				{
				$palabras[$j] = trim($palabras[$j]);
				
					if($palabras[$j]!='"')
					{
						if($text==true)
						{
						echo $palabras[$j];
						}
						else
						{
							
							foreach($variables as $var=>$tipo)
							{
								
								if($palabras[$j] == $var)
								{
									
									if($tipo == "int")
									{
										if(strpos($palabras[$j],"]")!==false)
										{
										}
										else
										{
										echo '%d';
										$vari[count($vari)+1] = $palabras[$j];
										}
									}
									
									if($tipo == "char")
									{
										if(strpos($palabras[$j],"]")!==false)
										{
										}
										else
										{
										echo '%c';
										$vari[count($vari)+1] = $palabras[$j];
										}
									}
									
									if($tipo == "float")
									{
										if(strpos($palabras[$j],"]")!==false)
										{
										}
										else
										{
										echo '%f';
										$vari[count($vari)+1] = $palabras[$j];
										}
									}
								}
								
								if(strpos($palabras[$j],$var)!==false)
								{
									if($tipo == "char *")
									{
										if(strpos($palabras[$j],"[")!==false)
										{
										echo '%c';
										$vari[count($vari)+1] = $palabras[$j];
										}
										else
										{
										echo '%s';
										$vari[count($vari)+1] = $palabras[$j];
										}
									}
									
									if($tipo == "int *")
									{
										if(strpos($palabras[$j],"[")!==false)
										{
										echo '%d';//habría que comprobar que la palabra tenga corchetes
										$vari[count($vari)+1] = $palabras[$j];
										}
										else
										{
										//Imprimir un error ya que no se puede imprimir un array de enteros
										}
										
									}
									
									if($tipo == "float *")
									{
										if(strpos($palabras[$j],"[")!==false)
										{
										echo '%f';//habría que comprobar que la palabra tenga corchetes
										$vari[count($vari)+1] = $palabras[$j];
										}
										else
										{
										//Imprimir un error ya que no se puede imprimir un array de float
										}
									}
								}
							}
						}
					
					}
					else
					{
						if($text == false)
						{
						$text = true;	
						}
						else
						{
						$text = false;	
						}
					}
					
				if($j<($n-1)){echo " ";}
				}
				
				
				if(count($vari) >=1)
				{
				echo '\n", ';
				
					for($c=1; $c<count($vari); $c++)
					{
						echo $vari[$c];
						echo ", ";
					}
				
				echo $vari[count($vari)];
				}
				else
				{
					echo '\n"';
				}
			
			//se ponen todos los nombres de las variables usadas en orden	
			echo ');';
			$i=$n;
			}
			else if($palabras[$i]=="LEER")
			{
				echo 'scanf("%';
				
				foreach($variables as $var=>$tipo)
				{
					$palabras[$i+1] = trim($palabras[$i+1]);
					if($palabras[$i+1] == $var)
					{
						if($tipo == "int")
						{
							echo 'd", &'.$palabras[$i+1].');';
						}
						
						if($tipo == "char")
						{
							echo 'c", &'.$palabras[$i+1].');';
						}
						
						if($tipo == "float")
						{
							echo 'f", &'.$palabras[$i+1].');';
						}
						
						if($tipo == "char *")
						{
							echo 's", '.$palabras[$i+1].');';
						}
						
						if($tipo == "int *")
						{
							echo 'd", '.$palabras[$i+1].');';
						}
						
						if($tipo == "float *")
						{
							echo 'd", '.$palabras[$i+1].');';
						}
					}
				}
				
			$i=$i+1;
			}
			
			//PARTE REFERENTE AL SI-///
			else if($palabras[$i]=="SI")
			{
			$nivel=$nivel+1;
			echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
			echo "if(";
				$n = count($palabras);
				for($j=($i+1); $j<$n; $j++)
				{
					//se procesa cada palabra dentro del SI
					if($palabras[$j]=="=")
					{
					echo "==";
					}
					
					else if($palabras[$j]=="MOD")
					{
						echo "%";
					}
					else
					{
						echo $palabras[$j];
					}
					if($j<($n-1)){echo " ";}
				}
			$i=$n;
			echo ")";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
			else if($palabras[$i]=="SINO")
			{
			echo "}";
			echo "</br>";
			tab($nivel);
			echo "else";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
			else if($palabras[$i]=="FIN-SI")
			{
			echo "}";
			$nivel=$nivel-1;
			}
			//PARTE REFERENTE AL PARA-///
			else if($palabras[$i]=="PARA")
			{
			$nivel=$nivel+1;
			echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
			echo "for(";
			
			//procesamos el PARA
			$n = count($palabras);
				for($j=($i+1); $j<$n; $j++)
				{
					//se procesa cada palabra dentro del PARA
					if($palabras[$j]=="HASTA")
					{
						echo ";";
					}
					else if($palabras[$j]=="INCREMENTO")
					{
						echo "; ";
						echo $palabras[1];
						echo " = ";
						echo $palabras[1];
						echo " + ";
					}
					else if($palabras[$j]=="DECREMENTO")
					{
						echo "; ";
						echo $palabras[1];
						echo " = ";
						echo $palabras[1];
						echo " - ";
					}
					else if($palabras[$j]=="MOD")
					{
						echo "%";
					}
					else
					{
						echo $palabras[$j];
					}
					if($j<($n-1)){echo " ";}
				}
			$i=$n;
			echo ")";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
			
			else if($palabras[$i]=="FIN-PARA")
			{
			echo "}";
			$nivel=$nivel-1;
			}
			
			
			//PARTE REFERENTE AL MIENTRAS///
			else if($palabras[$i]=="MIENTRAS")
			{
			$nivel=$nivel+1;
			echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
			echo "while(";
			$n = count($palabras);
				for($j=($i+1); $j<$n; $j++)
				{
					//se procesa cada palabra dentro del MIENTRAS
					if($palabras[$j]=="=")
					{
					echo "==";
					}
					
					else if($palabras[$j]=="MOD")
					{
						echo "%";
					}
					else
					{
						echo $palabras[$j];
					}
					if($j<($n-1)){echo " ";}
				}
			$i=$n;
			echo ")";
			echo "</br>";
			tab($nivel);
			echo "{";
			}
				
			else if($palabras[$i]=="FIN-MIENTRAS")
			{
				echo "}";
				$nivel=$nivel-1;
			}
			//PARTE REFERENTE A LAS OPERACIONES
			
			else if($palabras[$i]=="<-")
			{
				echo " = ";
			}
			//si la operacion es MOD
			else if($palabras[$i]=="MOD")
			{
				echo " % ";
				
			}
			//Si leemos un SALIR
			else if($palabras[$i]=="SALIR")
			{
				echo "exit(-1);";
			}
			else if($palabras[$i]=="DEVUELVE")
			{
				
				echo "return ";
				$palabras[$i+1] = trim($palabras[$i+1]);
				echo $palabras[$i+1];
				echo ";";
				$i=$i+1;
			}
				
			///------------------------------SE LLEGA AL FINAL DE LA FUNCIÓN---------------------------------///////
			else if($palabras[$i]=="FIN")
			{
			//Para poner las llaves que queden por cerrar
			echo "}";
			}
			else 
			{
				echo $palabras[$i]; 
				if ($i==$n-1 && $i>1)
				{
					echo ";";
				}
				echo " ";
			}

		}	
	//echo "////";
	$i=$i+1;
	}


if($par!=true && $func!=true) echo "</br>";

//echo $linea;
}


//FUNCIÓN PARA TABULAR
function tab($nivel)
{
	for($z=1;$z<=$nivel;$z++)
	{
		//echo "<dd><dd>";
		//echo "a ";
		echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
		
		
	}
}


//FUNCION PARA LOS CIERRES DEL NOMBRE DE LAS FUNCIONES
function cierreFun()
{
echo ")";
echo "</br>";
echo "{";	
}



//FUNCIÓN PARA GUARDAR TODAS LAS VARIABLES CON SUS RESPECTIVOS TIPOS
function tipoPalabras($palabra)
{
	
	if($palabra=="ENTERO")
	{
		echo "int ";
		$tipo="int";
	}
	
	if($palabra=="CARACTER")
	{
		echo "char ";
		$tipo="char";
	}
	
	if($palabra=="REAL")
	{
		echo "float ";
		$tipo="float";
	}
	
	if($palabra=="CADENA")
	{
		echo "char *";
		$tipo="char *";
	}
	
	if(strpos($palabra,"ARRAY")!==false)
	{
		if(strpos($palabra,"ENTERO")!==false)
		{
		echo "int *";
		$tipo="int *";
		}	
		
		if(strpos($palabra,"CARACTER")!==false)
		{
		echo "char *";
		$tipo="char *";
		}
		
		if(strpos($palabra,"REAL")!==false)
		{
		echo "float *";
		$tipo="float *";
		}
	}
	
return $tipo;
}





fclose($fp);
?>
</article>
</body>
</html>