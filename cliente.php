<?php 

error_reporting(0);

if($_GET["codigo"]=='C') {

	?><a href= http://localhost/Practicasoa/cliente.php><button>Volver</button></a>
	<br>
	<br>
	<?php
	echo '<h2 style="text-align: center; font-size:25pt;">Este es su pseudocodigo en C</h2>';
	 $data = array("fichero"=>$_GET["fichero"], "codigo"=>"C");
	 $handle = curl_init("http://localhost/PracticaSOA/esb.php");
	 curl_setopt($handle, CURLOPT_POST, true);
	 curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
	 curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	  $response=curl_exec($handle);
	 curl_close($handle);
	 echo $response;
	 ?>
	 <br>
	<br>
	 <a href= http://localhost/Practicasoa/cliente.php><button>Volver</button></a>
	
	<?php
	 
	 
}
else if($_GET["codigo"]=='PHP') {
	?><a href= http://localhost/Practicasoa/cliente.php><button>Volver</button></a>
	<br>
	<br>
	<?php
	echo '<h2 style="text-align: center; font-size:25pt;">Este es su pseudocodigo en PHP</h2>';
	 $data = array("fichero"=>$_GET["fichero"], "codigo"=>"PHP");
	 $handle = curl_init("http://localhost/PracticaSOA/esb.php");
	 curl_setopt($handle, CURLOPT_POST, true);
	 curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
	 curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	  $response=curl_exec($handle);
	 curl_close($handle);
	 echo $response;
	 ?><br>
	<br>
	<a href= http://localhost/Practicasoa/cliente.php><button>Volver</button></a>
	
	<?php
	 
	 
}
else if($_GET["codigo"]=='Perl') {
	?><a href= http://localhost/Practicasoa/cliente.php><button>Volver</button></a>
	<br>
	<br>
	<?php
	echo '<h2 style="text-align: center; font-size:25pt;">Este es su pseudocodigo en Perl</h2>';
	 $data = array("fichero"=>$_GET["fichero"], "codigo"=>"Perl");
	 $handle = curl_init("http://localhost/PracticaSOA/esb.php");
	 curl_setopt($handle, CURLOPT_POST, true);
	 curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
	 curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	  $response=curl_exec($handle);
	 curl_close($handle);
	 echo $response;
	 ?><br>
	<br><a href= http://localhost/Practicasoa/cliente.php><button>Volver</button></a>
	
	<?php
	 
	 
}
else
{?>

<html  lang="es">
<head>
  <meta charset="UTF-8">
  <title>Traductor a código</title>
</head>
<body>
<article style="padding-left:15%; padding-right:15%; text-align: justify;">
<h1 style="text-align: center; font-size:25pt;">Traductor de Pseudocodigo a C, PHP o Perl</h1>
<hr>
<h4 style="text-align: center;">Creado por:</h4>
<h3 style="text-align: center;">Adrián Barrios Aganzo</h3>
<h3 style="text-align: center;">Adrián Rodríguez Carneiro</h3>

<br>
<hr>
<p>Debe seleccionar el archivo en .txt que contiene el pseudocódigo (este debe estar en la misma carpeta que el traductor) y el lenguaje al que quiere traducir: C, PHP o Perl </p>

<p><i>Nota: si necesita ayuda sobre el formato que debe seguir el Pseudocodigo, lea nuestra <a href= http://localhost/Practicasoa/guia.html>Guía de Uso</a></i></p>

</br>
</br>

<form  style="text-align: center;" action="cliente.php" method="get" enctype="multipart/form-data"> 
   	<p><b>Seleccione el archivo .txt que contiene el pseudocodigo: </b> </p>
   	<p><input name="fichero" type="file"> </p>
   	<br>

   	<p><b>Seleccione el lenguaje al que quiere traducir el pseudocodigo: </b></p>
   	<p>C<input type ="Radio" name ="codigo" value="C"></p>
   	<p>PHP<input type ="Radio" name ="codigo" value="PHP"></p>
   	<p>Perl<input type ="Radio" name ="codigo" value="Perl"></p>
   	<p style="padding-bottom: 4%;"><input type="submit" value="Traducir"></p>
</form>
</article>
</body>
</html>

<?php
}
?>